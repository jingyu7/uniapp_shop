**uniapp 开发的一款移动商城**

接口请求数据为本地接口:/static/api/

* getlunbo.json    	获取轮播图
* getHotGoods.json 	获取首页商品列表
* getGoodDetail.json 	获取商品详情
* getNews.json     	获取资讯列表
* getNewsDetail.json 	获取资讯详情
* getImgCategory.json 获取图片分类
* getDateByCategory.json 根据图片分类获取图片列表

![](README_files/1.jpg)
![](README_files/2.jpg)